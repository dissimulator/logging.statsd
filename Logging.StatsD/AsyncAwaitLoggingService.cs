﻿namespace Logging.StatsD
{
    using System.Threading.Tasks;

    using Andy.French.Logging;

    using StatsdClient;

    /// <summary>
    /// This class writes to the log using async await.
    /// </summary>
    public class AsyncAwaitLoggingService : ILoggingService
    {
        /// <summary>The logger.</summary>
        private ILogger logger;

        /// <summary>
        /// Initialises a new instance of the <see cref="AsyncAwaitLoggingService"/> class.
        /// </summary>
        /// <param name="loggerFactory">The logger factory.</param>
        public AsyncAwaitLoggingService(ILoggerFactory loggerFactory)
        {
            this.logger = loggerFactory.CreateInstance(typeof(AsyncAwaitLoggingService));
        }

        /// <summary>
        /// Does the logging.
        /// </summary>
        /// <param name="numberOfTimes">The number of times to log.</param>
        public async void DoLogging(int numberOfTimes)
        {
            using (Metrics.StartTimer("time-to-log-async-await"))
            {
                for (int i = 0; i < numberOfTimes; i++)
                {
                    await Task.Run(() => this.logger.Debug("This is test message {0} from the AsyncAwaitLoggingService.", i));
                }
            }
        }
    }
}