﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Logging.StatsD")]
[assembly: AssemblyDescription("An application to tests StatsD and asynchronous logging.")]
[assembly: AssemblyCompany("Andy French")]
[assembly: AssemblyProduct("Logging.StatsD")]
[assembly: AssemblyCopyright("Copyright © Andy French 2015")]
[assembly: ComVisible(false)]
[assembly: Guid("c0d248f8-0ef1-4e35-a27d-e217e04bc15e")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif