﻿namespace Logging.StatsD
{
    using Andy.French.Configuration.Service;
    using Andy.French.Logging;
    using Andy.French.Logging.Log4Net;

    using Ninject.Modules;

    /// <summary>
    /// A <c>Ninject</c> module.
    /// </summary>
    public class ProgramModule : NinjectModule
    {
        /// <summary>
        /// Loads the module into the kernel.
        /// </summary>
        public override void Load()
        {
            this.Bind<ILoggerFactory>().To<LoggerFactory>();
            this.Bind<IConfigurationService>().To<AppSettingsConfigurationService>();
            this.Bind<ILoggingService>().To<SynchronousLoggingService>().Named("Synchronous");
            this.Bind<ILoggingService>().To<AsynchronousLoggingService>().Named("Asynchronous");
            this.Bind<ILoggingService>().To<AsyncAwaitLoggingService>().Named("AsyncAwait");
        }
    }
}