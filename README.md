# README #

### What is this repository for? ###

This repository contains a small test application to experiment with the [Log4Net.Async](https://github.com/cjbhaines/Log4Net.Async) library, as well as pushing metrics into statsd using the  [Statsd C# Client](https://github.com/goncalopereira/statsd-csharp-client). It is not production-ready code and is intended as a training tool only.

The code was writen to accompany a blog post ([ Testing Log4Net.Async with statsd and Graphite ](http://www.andyfrench.info/2015/04/testing-log4netasync-with-statsd-and.html)).

### How do I get set up? ###

* Simply clone the repository.
* I have included some NuGet packages that aren't available on a public NuGet server.

### Contribution guidelines ###

* This project is a demo only. No contributions are required.